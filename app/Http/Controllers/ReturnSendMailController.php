<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;
use App\Models\ReturnSendmail;

class ReturnSendMailController extends Controller
{
    public function getALL()
    {

        $list = ReturnSendmail::all();
        return response()->json($list);
    }

    public function get($id)
    {
        $result = ReturnSendmail::find($id);
        return response()->json($result);
    }

    public function update(Request $request, $id)
    {
        $data = ReturnSendmail::findOrFail($id);
        $data->update($request->all());
        return response()->json($data);
    }

    public function lichHen(\Illuminate\Http\Request $request, \Illuminate\Mail\Mailer $mailer)
    {
       $lichHen = new ReturnSendmail();
       $lichHen->name=$request->input('name');
        $lichHen->email=$request->input('email');
        $lichHen->phone=$request->input('phone');
        $lichHen->thongtinthem=$request->input('thongtinthem');
        $lichHen->ngayhen=$request->input('ngayhen');
        $lichHen->save();

        $mailer->to($request->input('email'))
            ->send(new \App\Mail\MyMail($request->input('thongtinthem')));

        return $request;
    }

}
